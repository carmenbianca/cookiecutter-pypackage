#!/bin/bash
# This script sets up a testing environment for the GitLab CI runner, and runs
# the tests. It requires the presence of a valid TOXENV environment variable.

if [ ! $TOXENV ]; then
    echo "Error: No TOXENV supplied."
    echo "Aborting."
    exit 0
fi

# Create a new virtualenv and use it.
# The reason we do this is to allow the installation of tox wihout root access.
# It's a little wasteful, as tox ends up creating its own virtualenv as well,
# but it's cheap enough.
virtualenv -p/usr/bin/python3 .virtualenvs/python3
source .virtualenvs/python3/bin/activate

# Print every command as it is executed. Would have included the virtualenv
# stuff as well, but virtualenv *itself* is full of bash commands, and I
# haven't figured out a way to censor those.
set -x

# Display runner hostname.
hostname

# Display current directory contents.
ls -la

# Install tox and run it.
pip install tox
tox
