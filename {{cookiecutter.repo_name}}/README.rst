===============================
{{ cookiecutter.project_name }}
===============================

.. image:: https://pypip.in/version/{{ cookiecutter.repo_name }}/badge.svg
    :target: https://pypi.python.org/pypi/{{ cookiecutter.repo_name }}
.. image:: https://pypip.in/py_versions/{{ cookiecutter.repo_name }}/badge.svg
    :target: https://pypi.python.org/pypi/{{ cookiecutter.repo_name }}
.. image:: https://pypip.in/format/{{ cookiecutter.repo_name }}/badge.svg
    :target: https://pypi.python.org/pypi/{{ cookiecutter.repo_name }}
.. image:: https://pypip.in/license/{{ cookiecutter.repo_name }}/badge.svg
    :target: https://www.gnu.org/copyleft/gpl.html


{{ cookiecutter.project_short_description}}

* Free software: GPLv3+
* Documentation: https://{{ cookiecutter.repo_name }}.readthedocs.org.

Features
--------

* TODO
